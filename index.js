const express = require('express')
const demoRouter = require('./server/demo/index')
const authorRouter = require('./server/author/index')
const sharedspaces = require('./server/sharedspaces')
const bodyParser = require('body-parser');

const app = express();

// create application/json parser
app.use(bodyParser.json());
// create application/x-www-form-urlencoded parser
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/api/demo', demoRouter);
app.use('/api/author', authorRouter);
app.use('/api', sharedspaces);

app.listen(8080,() => {
    console.log('api server running at http://127.0.0.1:8080')
})
