const express = require('express')
const router = express.Router()
const demo_handler = require('./demo.controller')

router.get('/create',demo_handler.getCreate())

router.get('/getList',demo_handler.getDemolist())

module.exports = router