const demoModel = require('./demo.model')
const authorModel = require('../author/author.modul')
const response = require('../../utils/res')

class demo {
    getCreate() {
        return async (req, res) => {
            let temp = req.body;
            console.log(temp);
            let test = {};
            for(let i=0; i<5; i++) {
                let flag = Math.ceil(Math.random()*100);
                test = {
                    'description': flag >= 50 ? 'PHP是一种创建动态交互性站点的强有力的服务器端脚本语言。':'Java是由SunMicrosystems公司于1995年5月推出的高级程序设计语言。',
                    'title': 'PHP教程'+flag,
                    'url': 'http://www.runoob'+flag+'.com',
                    'by': '菜鸟教程'+Math.ceil(Math.random()*100),
                    'tags': flag >= 50 ? 'php':'java',
                    'likes': flag
                }
                const obj = await demoModel.create(test);
                console.log(obj)
            }
            res.send('haha')
        }
    }

    getDemolist() {
        return async (req, res) => {
            try {
                const bylist = [];
                const obj = await demoModel.find({},'_id by');
                obj.map(v => {
                    bylist.push(v.by)
                });
                const demotest = await demoModel.find();
                const authortest = await authorModel.find({'name':{$in:bylist}});
                console.log('-------------authortest-----------');
                console.log(authortest);
                // const authortest1 = await authorModel.find({'name':bylist})
                console.log('-------------obj-----------');
                console.log(obj);
                for (const demotestElement of demotest) {
                Object.assign(demotestElement,{
                    'author':authortest
                })
                }
                response(res,true,'查询成功',obj)
            } catch (e) {
                console.log(e)
                response(res,false,e,null)
            }

        }
    }
}

module.exports = new demo()