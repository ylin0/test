const mongoose = require('mongoose')
const Schema = mongoose.Schema
const db = require('../../db/mongo')


const demo = new Schema({
    description:String,
    likes:Number,
    tags:Array,
    title:String,
    url:String,
    by:String
}, {
    _id:true,
    versionKey: false,
    timestamps: true
})

module.exports = db.model('demo', demo)