const mongoose = require('mongoose');
const db = require('../../db/mongo');
const Schema = mongoose.Schema;

const author = new Schema({
    name:String,
    age:Number,
    sex:String,
    department:Array
},{
    _id:true,
    versionKey: false,
    timestamps: true
})

module.exports = db.model('author',author);