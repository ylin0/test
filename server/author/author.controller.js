const authorModul = require('./author.modul');
const response = require('../../utils/res')

class author{
    getCreate() {
       return async (req, res, next) => {
           let temp = {};
           for(let i=0; i<5; i++) {
               let flag = Math.ceil(Math.random()*100);
               temp = {
                   'name': '菜鸟教程' + flag,
                   'age': flag,
                   'sex': flag >= 50 ? '男':'女',
                   'department': '技术部' + flag,
               };
               const obj = await authorModul.create(temp);
               console.log(obj);
           }
           res.send('author');
       }
    }

    getList() {
        return async (req, res, next) => {
            const data_v = await authorModul.find();
            response(res, true, '获取信息成功', '');
        }
    }

}

module.exports = new author();
