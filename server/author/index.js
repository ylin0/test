const express = require('express')
const authorHandle = require('./author.controller')
const demoHandle = require('../demo/demo.controller')


const router = express.Router()


router.get('/create',authorHandle.getCreate());

router.get('/getList',authorHandle.getList());

module.exports = router
