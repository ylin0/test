const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const db = require('../../db/mongo');

// const resource = new Schema({
//     ref_id:mongoose.Types.ObjectId,
//     name: String,
//     state: Number,
//     type: Number,
//     createdBy:String,
//     updateBy:String,
//     dir: Array
// },{
//     _id:true,
//     versionKey: false,
//     timestamps: true
// });

const sharedspace = new Schema({
    dir:Array,
    createdBy:String,
    updateBy:String,
},{
    _id:true,
    versionKey: false,
    timestamps: true
});

module.exports = db.model('sharedspace',sharedspace);
