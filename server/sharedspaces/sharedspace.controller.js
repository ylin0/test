const lodash = require('lodash')
const sharedspaceModel = require('./sharedspace.model');
const response = require('../../utils/res');
const {ObjectId} = require("mongoose/lib/types");


class sharedspace {

    init() {
        return async (req, res, next) => {
            try {
                let userName = 'yanl';
                const sharedDate = await sharedspaceModel.findOne({'createdBy' : userName});
                if(!lodash.isEmpty(sharedDate)) {
                    response(res, false, '已经初始化', null);
                    return;
                }
                const personalSpace = {
                    dir: [],
                    createdBy:userName,
                    updateBy:userName
                };
                sharedspaceModel.create(personalSpace);
                response(res, true, '初始化成功', null);
            } catch (e) {
                console.log(e);
                response(res, false, '初始化失败', null);
            }
        };
    }

    create() {
        return async (req, res, next) => {
            try {
                let {
                    ref_id,
                    name,
                    state,
                    type,
                    filePath
                } = req.query;
                let userName = 'yanlin';
                //判断空间是否初始化
                let sharedData = await sharedspaceModel.findOne({ createdBy: userName});
                if(lodash.isEmpty(sharedData)) {
                    response(res, false, '空间未初始化', '');
                    return;
                }
                let fileName = filePath.split('/');
                //去除空字符
                fileName = fileName.filter(item=> item);
                if('/' !== filePath[0] || fileName.length == 0) {
                    response(res, false, '文件路径不规范', '');
                    return;
                }
                const date_v = new Date();
                if(userName == fileName[0]) {
                    let file = {
                        '_id':ObjectId(),
                        'ref_id':ref_id,
                        'name':name,
                        'state':parseInt(state),
                        'type':parseInt(type),
                        'dir':[],
                        'updatedBy':userName,
                        'createdBy':userName,
                        'updatedAt':date_v,
                        'createdAt':date_v};
                    //定位到新增文件的文件夹，并修改updatedBy的时间
                    let tempArray=[];
                    let tempObject=sharedData;
                    for(let i=1;i<fileName.length;i++) {
                        tempArray = tempObject.dir;
                        for(let j=0;j<tempArray.length;j++){
                            if(fileName[i] == tempArray[j].name) {
                                tempObject = tempArray[j];
                                break;
                            }
                        }
                        tempObject.updatedAt = date_v;
                    }
                    //判断新增文件的路径是否为文件夹
                    if(fileName.length !== 1 && tempObject.type !== 1) {
                        response(res, false, '新增失败，'+ tempObject.name+'不是文件夹');
                        return;
                    }
                    tempObject.dir.push(file);
                    sharedData.markModified('dir');
                    sharedData.save();
                    response(res, true, '新建成功', '');
                } else {
                    response(res, false, '文件不能保存到他人空间', '');
                    return;
                }
            } catch (e) {
                console.log(e);
                response(res, false, '新建失败', null)
            }
        };
    }

    getfileList() {
        return async (req, res, next) => {
            try {
                let userName = 'yanlin';
                const sharedDate= await sharedspaceModel.findOne({'createdBy' : userName});
                if (lodash.isEmpty(sharedDate)) {
                    response(res, false, '空间未初始化', '');
                }
                response(res, true, '获取文件信息成功', sharedDate);
            } catch (e) {
                console.log(e);
                response(res, false, '获取文件信息失败', '');
            }
        };
    }

}

module.exports = new sharedspace();
