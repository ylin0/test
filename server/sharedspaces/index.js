const express = require('express');
const router = express.Router();
const sharedspaces = require('./sharedspace.controller');


/**
 * 初始化个人空间
 */
router.route('/sharedspace/myspace/init')
    .post(sharedspaces.init());

/**
 * 新建文件
 */
router.route('/sharedspace/myspace')
    .post(sharedspaces.create());

/**
 * 查看我的空间
 */
router.route('/sharedspace/myspace')
    .get(sharedspaces.getfileList());


module.exports = router;
