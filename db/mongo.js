const mongo = require('mongoose')
const log = require('../utils/debugLogger')

const options = {
    poolSize: 10,
    connectTimeoutMS:20000,
    bufferCommands: false,
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true
};


const db = mongo.createConnection('mongodb://root:root@127.0.0.1:27017/server-dev',options)


db.on('connecting', () => {
    log.debug(('正在建立数据库连接'));
});

db.on('connected', () => {
    log.debug(`数据库连接成功\tmongodb://root:root@127.0.0.1:27017/server-dev}`);
});

db.on('error', (err) => {
    log.debug((`数据库连接失败 ${err}`));
});

db.on('disconnected', () => {
    log.debug('数据连接已关闭');
});

module.exports = db
