const chalk = require('chalk');
const moment = require('moment');
const log = console.log;

class DebugLogger {
    constructor() {
    }

    info(msg) {
        chalk.level = 1;
        log(chalk.blue(moment().format('YYYY-MM-DD hh:mm:sss')+"\t"+msg));
    }
    
    debug(msg) {
        chalk.level = 1;
        log(chalk.green(moment().format('YYYY-MM-DD hh:mm:sss')+"\t"+msg));
    }

    warn(msg) {
        chalk.level = 1;
        log(chalk.yellow(moment().format('YYYY-MM-DD hh:mm:sss')+"\t"+msg));
    }
    
    error(msg){
        chalk.level = 1;
        log(chalk.red(moment().format('YYYY-MM-DD hh:mm:sss')+"\t"+msg));
    }
}

module.exports = new DebugLogger();