
const response = function (res,success = true,mag = '',data = null) {
    const result = {
        success:success,
        mag:mag,
        data:data
    };
    return res.json(result);
}

module.exports = response